#ifndef MENUWINDOW_H
#define MENUWINDOW_H

#include <QWidget>
#include "numbermemwindow.h"
#include "reactiontimewindow.h"
#include "visualmemorywindow.h"
#include "hearingtestwindow.h"
#include "aimtestwindow.h"
#include "user.h"

#include<QtCharts>
#include<QChartView>
#include<QScatterSeries>

QT_BEGIN_NAMESPACE
namespace Ui { class MenuWindow; }
QT_END_NAMESPACE


class UserWindow ;
class MenuWindow : public QWidget
{
    Q_OBJECT

public:
    MenuWindow(QWidget *parent = nullptr);
    ~MenuWindow();
    void setGames(NumberMemWindow &NumberMemGame ,
                  ReactionTimeWindow &ReactionTimeGame,
                  //VisualMemoryWindow &VisualMemoryGame,
                  aimtestwindow &AimTest,
                  hearingtestwindow &HearingTest);
    User *user ;
    void setUser(User &user) ;
    void setNameLabelUser() ;
    void setChart() ;
    void setUserWindow(UserWindow &uwindow) ;



private slots:
    void on_BtnMemNum_clicked();

    void on_BtnReactionTime_clicked();

    void on_BtnAimTest_clicked();


    void on_BtnHearingTest_clicked();

    void on_BtnHearingTest_2_clicked();

private:
    Ui::MenuWindow *ui;
    NumberMemWindow *NumberMemGame ;
    ReactionTimeWindow *ReactionTimeGame ;
    //VisualMemoryWindow *VisualMemoryGame ;
    hearingtestwindow *HearingTest ;
    aimtestwindow *AimTest;

    UserWindow *uwindow ;

    //agreagdo  de charts
    QLineSeries *rmberS0 ;
    QLineSeries *rmberS1 ;
    QAreaSeries *rmberSeries ;

    QChart *chartRmberNumber ;

    //agregado chart de hearing test
    QLineSeries *hearingS0 ;
    QLineSeries *hearingS1 ;
    QAreaSeries *hearingSeries ;

    QChart *chartHearing ;

    //agregado
    QLineSeries *reactionS0 ;
    QLineSeries *reactionS1 ;
    QAreaSeries *reactionSeries ;

    QChart *chartReaction ;

    //agregado aimtest
    QLineSeries *reactionS00 ;
    QLineSeries *reactionS11 ;
    QAreaSeries *reactionSeries1 ;

    QChart *chartReaction1 ;


};
#endif // MENUWINDOW_H
