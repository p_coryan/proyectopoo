#include "numbermemwindow.h"
#include "ui_numbermemwindow.h"
#include <string>
#include "menuwindow.h"


NumberMemWindow::NumberMemWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NumberMemWindow)
{
    ui->setupUi(this);
    countLvl = 1 ;
    countBar = 0  ;
    myRand = QRandomGenerator::securelySeeded();

    ui->inputNumber->setVisible(false);
    ui->lblText->setVisible(false) ;
    ui->BtnOK->setVisible(false) ;
    ui->InstruccionesTxt->setVisible(true) ;


    // barra de progreso
    ui->timeProgress->setRange(0,100) ;
    ui->timeProgress->setValue(countBar);
    ui->timeProgress->setVisible(false) ;
    ui->timeProgress->setTextVisible(false);

    //lvl label
    ui->lblLevel->setVisible(false) ;
    ui->lblLost->setVisible(false) ;

    //Botones cuando se pierde
    ui->BtnRestart->setVisible(false) ;
    ui->BtnExit->setVisible(false) ;

    // relacioando  con timers
    timer = new QTimer(this) ;
    timer->setInterval(30) ;
    connect(timer, SIGNAL(timeout()),
                this, SLOT(setProgressBar()));

    //agregado  btn ok
    ui->BtnOK->setVisible(false) ;



}

NumberMemWindow::~NumberMemWindow()
{
    delete ui;
    delete timer ;
}

void NumberMemWindow::setLevel(){
    numberLvl="" ;
    QString textLevel ="Level " ;
    textLevel += QString::number(countLvl) ;
    ui->lblLevel->setText(textLevel) ;
    for(int i = 0 ;  i< countLvl ;  i++){
        if(i== 0){ // para que el primero no sea  0
            int x = myRand.generateDouble()*9 + 1 ;
            numberLvl += QString::number(x);
        }else{
            int x = myRand.generateDouble()*10 ;
            numberLvl += QString::number(x);
        }

    }
    ui->inputNumber->setText("");

}

void NumberMemWindow::setProgressBar(){
    countBar ++ ;
     if(countBar<=100){
         ui->timeProgress->setValue(countBar);
         ui->inputNumber->setVisible(false) ;
     }else{
         timer->stop();
         ui->timeProgress->setVisible(false) ;
         ui->numberDisplay->setVisible(false) ;
         ui->inputNumber->setVisible(true) ;

         // agregado  Btn ok.
         ui->BtnOK->setVisible(true) ;

         countBar=0 ;
     }

}

void NumberMemWindow::on_BtnOK_clicked()
{

 if(ui->inputNumber->text() == numberLvl){
     countLvl ++ ;
     this->setLevel() ;
     ui->numberDisplay->setText(numberLvl)  ;  //display(numberLvl) ;
     ui->numberDisplay->setVisible(true) ;
     ui->timeProgress->setVisible(true) ;
     timer->start() ;

     //agregado  btn ok
     ui->BtnOK->setVisible(false) ;
 }else{
     //menu de salida (score , exit y restart)
     ui->BtnRestart->setVisible(true) ;
     ui->BtnExit->setVisible(true) ;
     ui->lblLost->setVisible(true) ;
     QString LostText = "    You Lost!!!\n  Your Score : Max level " ;
     LostText+= QString::number(countLvl) ;
     ui->lblLost->setText(LostText) ;

     //menu de incio de deshabilita
     ui->BtnOK->setVisible(false) ;
     ui->inputNumber->setVisible(false) ;
     ui->lblLevel->setVisible(false) ;
     ui->lblText->setVisible(false) ;
     ui->numberDisplay->setVisible(false) ;

     // agregar aca hight score
     if(countLvl > MaxScore){
         MaxScore = countLvl ;
         this->menu->user->setScoreNumberMem(MaxScore);
         this->menu->user->writeFileUser();
         this->menu->setChart();
     }

 }
}


void NumberMemWindow::on_BtnStart_clicked()
{
    ui->numberDisplay->setVisible(true) ;
    ui->InstruccionesTxt->setVisible(false) ;
    ui->inputNumber->setVisible(true) ;
    ui->lblText->setVisible(true) ;
    ui->BtnOK->setVisible(false) ; // cambio de hoy de btn ok
    ui->timeProgress->setVisible(true) ;
    timer->start() ;
    ui->BtnStart->setVisible(false) ;
    ui->lblLevel->setVisible(true) ;
    ui->lblLevel->setText("level 1")  ;
    int x = myRand.generateDouble()*9 + 1 ;
    numberLvl += QString::number(x);
    ui->numberDisplay->setText(numberLvl)  ;


}


void NumberMemWindow::on_BtnRestart_clicked()
{
    countLvl=1 ;
    numberLvl="" ;
    ui->inputNumber->setText("");
    //desahbilitar el menu de score
    ui->BtnRestart->setVisible(false) ;
    ui->BtnExit->setVisible(false) ;
    ui->lblLost->setVisible(false) ;


    // habilitar menu de incio y conciciones inciales
    ui->BtnOK->setVisible(false) ; // en esta parte tambien se cambio el btn OK
    ui->inputNumber->setVisible(true) ;
    ui->lblText->setVisible(true) ;
    ui->numberDisplay->setVisible(true) ;
    ui->timeProgress->setVisible(true) ;
    timer->start() ;
    ui->lblLevel->setVisible(true) ;
    ui->lblLevel->setText("level 1")  ;
    int x = myRand.generateDouble()*9 + 1 ;
    numberLvl += QString::number(x);
    ui->numberDisplay->setText(numberLvl)  ;


    this->setScore() ;
}


void NumberMemWindow::on_BtnExit_clicked()
{
   countLvl = 1 ;
   numberLvl="" ;
   ui->inputNumber->setText("");

   countBar = 0  ;

   ui->inputNumber->setVisible(false);
   ui->lblText->setVisible(false) ;
   ui->BtnOK->setVisible(false) ;
   ui->InstruccionesTxt->setVisible(true) ;
   ui->BtnStart->setVisible(true) ;

    ui->timeProgress->setVisible(false) ;
    ui->lblLevel->setVisible(false) ;
    ui->lblLost->setVisible(false) ;

    ui->BtnRestart->setVisible(false) ;
    ui->BtnExit->setVisible(false) ;

   this->close() ;
}

void NumberMemWindow::setOpenGame(){
    countLvl = 1 ;
    countBar = 0  ;
    numberLvl="" ;
    ui->inputNumber->setText("");
    ui->inputNumber->setVisible(false);
    ui->lblText->setVisible(false) ;
    ui->InstruccionesTxt->setVisible(true) ;


    // barra de progreso
    ui->timeProgress->setRange(0,100) ;
    ui->timeProgress->setValue(countBar);
    ui->timeProgress->setVisible(false) ;
    ui->timeProgress->setTextVisible(false);

    //lvl label
    ui->lblLevel->setVisible(false) ;
    ui->lblLost->setVisible(false) ;

    //Botones cuando se pierde
    ui->BtnRestart->setVisible(false) ;
    ui->BtnExit->setVisible(false) ;

    //agregado  btn ok
    ui->BtnOK->setVisible(false) ;
    ui->BtnStart->setVisible(true) ;

}

void NumberMemWindow::setMenu(MenuWindow &menu){
    this->menu = &menu ;
}

void NumberMemWindow::setScore(){
    MaxScore = this->menu->user->getScoreNumberMem() ;
}


// hacer metodo para cuando se cierre para tener valor inciles de
//cundo se incia (se podria hacer en el mismo exit

