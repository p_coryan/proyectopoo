#ifndef AIMTESTWINDOW_H
#define AIMTESTWINDOW_H

#include <QWidget>
#include <QRandomGenerator>
#include <QTimer>
#include <QMouseEvent>
#include <QString>
#include <QPainter>
#include <QGraphicsScene>
#include "circlemove.h"
#include <QGraphicsView>

class MenuWindow ;
namespace Ui {
class aimtestwindow;
}

class aimtestwindow : public QWidget
{
    Q_OBJECT

public:
    explicit aimtestwindow(QWidget *parent = nullptr);
    ~aimtestwindow();
    void mousePressEvent(QMouseEvent *event);
    void Restart();
    void setMenu(MenuWindow &menu) ;
    void setScore();

public slots:
    void timeUpdate();


private slots:
    void on_BtnReset_clicked();

private:
    Ui::aimtestwindow *ui;
    QTimer *timer;
    QRandomGenerator myRand;
    bool isPlaying;
    bool isFinished;
    int time;
    int count;
    int x, y, r;

    void Start();
    QGraphicsScene *scene;
    Circlemove *circle;
    MenuWindow *menu ;
    double maxScore ;


};

#endif // AIMTESTWINDOW_H
