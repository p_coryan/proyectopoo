#include "circlemove.h"


Circlemove::Circlemove(int X, int Y, int ancho,
                       int alto)
    :QGraphicsEllipseItem(X,Y,ancho,alto)
{
    myRand = QRandomGenerator::securelySeeded();
    rand = myRand.generateDouble();
}

void Circlemove::move(int x, int y){
    rand = myRand.generateDouble();
    if(rand<0.25){
        this->setX(x);
        this->setY(y);
    }
    else if(rand<0.5 && rand>0.25 ){
        this->setX(x);
        this->setY(-y);}
    else if(rand< 0.75 && rand>0.5 ){
        this->setX(-x);
        this->setY(y);}
    else {
        this->setX(-x);
        this->setY(-y);}
}
