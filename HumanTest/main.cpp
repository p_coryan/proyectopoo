#include "menuwindow.h"
#include "numbermemwindow.h"
#include "reactiontimewindow.h"
#include "visualmemorywindow.h"
#include "hearingtestwindow.h"
#include "aimtestwindow.h"
#include "userwindow.h"


#include <QApplication>
#include <QDir>
#include <QDebug>
#include <QString>
#include <QFile>
#include "user.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    /*el siguiente codigo es para guardar los datos localmente de los ususarios */

    UserWindow usersW ;
    MenuWindow Home;
    NumberMemWindow NumberMemGame ;
    ReactionTimeWindow ReactionTimeGame ;
    hearingtestwindow HearingTest ;
    aimtestwindow AimTest;

    User user ;

    NumberMemGame.setMenu(Home) ; // para actulizar graficas
    HearingTest.setMenu(Home) ; // para acceder a usurio
    ReactionTimeGame.setMenu(Home) ;
    AimTest.setMenu(Home) ;

    Home.setGames(NumberMemGame , ReactionTimeGame , AimTest , HearingTest) ;
    Home.setUser(user) ;
    Home.setUserWindow(usersW) ;
    usersW.setHome(Home) ;
    usersW.show() ;

    return a.exec();
}
