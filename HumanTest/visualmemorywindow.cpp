#include "visualmemorywindow.h"
#include "ui_visualmemorywindow.h"

VisualMemoryWindow::VisualMemoryWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VisualMemoryWindow)
{
    ui->setupUi(this);
}

VisualMemoryWindow::~VisualMemoryWindow()
{
    delete ui;
}
