#ifndef CIRCLEMOVE_H
#define CIRCLEMOVE_H

#include <QGraphicsEllipseItem>
#include <QWidget>
#include <QRandomGenerator>


class Circlemove:public QObject,public QGraphicsEllipseItem{
    Q_OBJECT
   public:
    Circlemove(int x, int y, int w, int h);
    void move(int x, int y);
   private:
    QRandomGenerator myRand;
    double rand;

};
#endif // CIRCLEMOVE_H
