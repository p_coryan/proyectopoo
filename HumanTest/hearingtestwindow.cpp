#include "hearingtestwindow.h"
#include "ui_hearingtestwindow.h"
#include "menuwindow.h"

#define MAX_FREQ 23000;
#define D 10000; //ms

hearingtestwindow::hearingtestwindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::hearingtestwindow)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    sweep = new QMediaPlayer;
    filePath = QDir::homePath();
    isHearing = false;
    freq = 23000;
    ui->Fclabel->setVisible(false);
    ui->hearBtn->setVisible(false);
    sweep->setMedia(QUrl("qrc:/Sounds/reverse_sweep.wav"));
    sweep->setVolume(40);
    ui->tryBtn->setVisible(false);
    connect(timer, SIGNAL(timeout()),this,SLOT(updatefreq()));

}

hearingtestwindow::~hearingtestwindow()
{
    delete ui;
}

void hearingtestwindow::on_hearBtn_clicked()
{
    if(!isHearing){
        sweep->play();
        timer->start(10);
        isHearing = true;
    }
    else{
        timer->stop();
        sweep->stop();
        ui->tryBtn->setVisible(true);

        // agregar aca hight score
        if( freq < maxScore){
            maxScore = freq ;
            this->menu->user->setScoreHearingTest(maxScore);
            this->menu->user->writeFileUser();
            this->menu->setChart();
        }
    }


}

void hearingtestwindow::on_startBtn_clicked()
{
    ui->Fclabel->setVisible(true);
    ui->hearBtn->setVisible(true);
    ui->instlabel->setVisible(true);
    ui->hearBtn->setText("Lo escucho");
    ui->instlabel->setText("Haz click cuando escuches el pitido");
    ui->startBtn->setVisible(false);
    if(!isHearing){

        sweep->play();
        timer->start(10);
        isHearing = true;
    }
    else{
        timer->stop();
        sweep->stop();

    }

    this->setScore() ;

}


void hearingtestwindow::on_tryBtn_clicked()
{
   reset();
}

void hearingtestwindow::updatefreq(){
    freq = freq - 20;
    if(freq<0){freq = 0;}
    ui->Fclabel->setText(QString::number(freq));

}

void hearingtestwindow::reset(){
    freq = 23000;
    isHearing = false;
    timer->stop();
    sweep->stop();
    ui->Fclabel->setVisible(false);
    ui->hearBtn->setVisible(false);
    ui->tryBtn->setVisible(false);
    ui->instlabel->setText("Test Auditivo");
    ui->startBtn->setVisible(true);

    // agregado
    this->setScore() ;

}

void hearingtestwindow::setMenu(MenuWindow &menu){
    this->menu = &menu ;
}

void hearingtestwindow::setScore(){
    maxScore =  this->menu->user->getScoreHearingTest() ;
}
