#ifndef USER_H
#define USER_H

#include <QString>
#include <iostream>
#include <fstream>

using namespace std ;


class User
{
public:
    User();
    //~User();

    void readFileUser() ;
    void writeFileUser() ;
    void setPathUser(QString path) ;
    void resetScore() ;
    void setNameUser(QString nameUser) ;
    QString getNameUser() ;

    double getScoreNumberMem ();
    double getScoreRectionTime() ;
    double getScoreHearingTest() ;
    double getScoreAIM() ;

    void setScoreNumberMem(double newscore);
    void setScoreReactionTime(double newscore) ;
    void setScoreHearingTest(double newscore) ;
    void setScoreAIM(double newscore) ;


private:
    QString path ;
    double scoreNumberMem ;
    double scoreReactionTime ;
    double scoreHearingTest ;
    double scoreAIM ;
    QString nameUser ;

};

#endif // USER_H
