#include "menuwindow.h"
#include "ui_menuwindow.h"
#include "userwindow.h"

MenuWindow::MenuWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MenuWindow)
{
    ui->setupUi(this);

    // agregado de charts
    rmberS0 = new QLineSeries() ;
    rmberS1 = new QLineSeries() ;
    rmberSeries = new QAreaSeries(rmberS0 , rmberS1);

    chartRmberNumber = new QChart ;
    chartRmberNumber->addSeries(rmberSeries);
    //chart->setTitle("Simple areachart example");
    chartRmberNumber->createDefaultAxes();
    chartRmberNumber->axes(Qt::Horizontal).first()->setRange(0, 3);
    chartRmberNumber->axes(Qt::Vertical).first()->setRange(0, 20);

    //gregdo de hearing test
    hearingS0= new QLineSeries() ;
    hearingS1= new QLineSeries() ;
    hearingSeries = new QAreaSeries(hearingS0 , hearingS1);

    chartHearing =  new QChart ;
    chartHearing ->addSeries(hearingSeries) ;
    chartHearing->createDefaultAxes();
    chartHearing->axes(Qt::Horizontal).first()->setRange(0, 3);
    chartHearing->axes(Qt::Vertical).first()->setRange(0, 23000);

    //agregado  de reaction time

    reactionS0 = new QLineSeries() ;
    reactionS1 = new QLineSeries() ;
    reactionSeries = new QAreaSeries(reactionS0 , reactionS1) ;

    chartReaction = new QChart ;
    chartReaction->addSeries(reactionSeries);
    chartReaction->createDefaultAxes();
    chartReaction->axes(Qt::Horizontal).first()->setRange(0, 3);
    chartReaction->axes(Qt::Vertical).first()->setRange(0, 1200);

    //agregado  de aim test

    reactionS00 = new QLineSeries() ;
    reactionS11 = new QLineSeries() ;
    reactionSeries1 = new QAreaSeries(reactionS00 , reactionS11) ;

    chartReaction1 = new QChart ;
    chartReaction1->addSeries(reactionSeries1);
    chartReaction1->createDefaultAxes();
    chartReaction1->axes(Qt::Horizontal).first()->setRange(0, 3);
    chartReaction1->axes(Qt::Vertical).first()->setRange(0, 900);




}

MenuWindow::~MenuWindow()
{
    delete ui;
}

void MenuWindow::setGames(NumberMemWindow &NumberMemGame, ReactionTimeWindow &ReactionTimeGame,
                          //VisualMemoryWindow &VisualMemoryGame,
                          aimtestwindow &AimTest, hearingtestwindow &HearingTest ){
    this->NumberMemGame =  &NumberMemGame ;
    this->ReactionTimeGame = &ReactionTimeGame ;
    //this->VisualMemoryGame =  &VisualMemoryGame ;
    this->HearingTest = &HearingTest;
    this->AimTest = &AimTest;
}


void MenuWindow::on_BtnMemNum_clicked()
{

     this->NumberMemGame->setScore();
     this->NumberMemGame->setOpenGame() ;
     this->NumberMemGame->show() ;
}


void MenuWindow::on_BtnReactionTime_clicked()
{
    this->ReactionTimeGame->setScore() ;
    this->ReactionTimeGame->show();

}


void MenuWindow::on_BtnAimTest_clicked()
{
    this->AimTest->Restart();
    this->AimTest->setScore();
    this->AimTest->show();

}


void MenuWindow::on_BtnHearingTest_clicked()
{
    this->HearingTest->reset();
    this->HearingTest->show() ;
}

void MenuWindow::setUser(User &user){
    this->user = &user ;
}

void MenuWindow::setNameLabelUser(){
    QString name = "Estadisticas de " + this->user->getNameUser() + ":" ;
    ui->lblUserName->setText(name) ;
}

void MenuWindow::setChart(){
    //rmberSeries->setName("Score Rember number") ;
    rmberS0->clear();
    rmberS1->clear();
    QPen pen(0x000009) ;
    pen.setWidth(3);
    rmberSeries->setPen(pen) ;

    *rmberS0 << QPointF(1 , this->user->getScoreNumberMem()) ;
    *rmberS1 << QPointF(2 ,this->user->getScoreNumberMem() ) ;

    *rmberS0 << QPointF(1 , 0) ;
    *rmberS1 << QPointF(2 ,0 ) ;

    this->ui->ChartRmbNumer->setChart(chartRmberNumber);
    this->ui->ChartRmbNumer->setRenderHint(QPainter::Antialiasing);

    // chart de hearing test

    hearingS0->clear() ;
    hearingS1->clear() ;
    QPen pen2(0x000009) ;
    pen2.setWidth(3) ;
    hearingSeries->setPen(pen2) ;

    *hearingS0 <<QPointF(1 , this->user->getScoreHearingTest()) ;
    *hearingS1 <<QPointF(2 , this->user->getScoreHearingTest()) ;

    *hearingS0 <<QPointF(1 , 0) ;
    *hearingS1 <<QPointF(2 , 0) ;

    this->ui->ChartHear->setChart(chartHearing) ;
    this->ui->ChartHear->setRenderHint(QPainter::Antialiasing) ;

    this->ui->labelMinFreq->setText(QString::number(this->user->getScoreHearingTest())) ;

    // chart de reaction  test

    reactionS0->clear() ;
    reactionS1->clear() ;
    QPen pen3(0x000009) ;
    pen3.setWidth(3) ;
    reactionSeries->setPen(pen3) ;

    *reactionS0 <<QPointF(1 , this->user->getScoreRectionTime()) ;
    *reactionS1 <<QPointF(2 , this->user->getScoreRectionTime()) ;

    *reactionS0 <<QPointF(1 , 0) ;
    *reactionS1 <<QPointF(2 , 0) ;

    this->ui->ChartReaction->setChart(chartReaction) ;
    this->ui->ChartReaction->setRenderHint(QPainter::Antialiasing) ;

    this->ui->labelMinTime->setText(QString::number(this->user->getScoreRectionTime())) ;

    // chart de aim test

    reactionS00->clear() ;
    reactionS11->clear() ;
    QPen pen4(0x000009) ;
    pen4.setWidth(3) ;
    reactionSeries1->setPen(pen4) ;

    *reactionS00 <<QPointF(1 , this->user->getScoreAIM()) ;
    *reactionS11 <<QPointF(2 , this->user->getScoreAIM()) ;

    *reactionS00 <<QPointF(1 , 0) ;
    *reactionS11 <<QPointF(2 , 0) ;

    this->ui->ChartReaction_2->setChart(chartReaction1) ;
    this->ui->ChartReaction_2->setRenderHint(QPainter::Antialiasing) ;

    this->ui->labelMinTimeAim->setText(QString::number(this->user->getScoreAIM())) ;




}


void MenuWindow::on_BtnHearingTest_2_clicked()
{
    this->uwindow->show();
    this->close();

}

void MenuWindow::setUserWindow(UserWindow &uwindow){
    this->uwindow= &uwindow ;
}
