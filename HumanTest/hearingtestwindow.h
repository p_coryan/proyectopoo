#ifndef HEARINGTESTWINDOW_H
#define HEARINGTESTWINDOW_H

#include <QWidget>
#include <QMediaPlayer>
#include <QTimer>
#include <QUrl>
#include <QDir>
#include<QString>
#include <QFile>
class MenuWindow ;
namespace Ui {
class hearingtestwindow;
}

class hearingtestwindow : public QWidget
{
    Q_OBJECT

public:
    explicit hearingtestwindow(QWidget *parent = nullptr);
    ~hearingtestwindow();
    void reset();
    void setMenu(MenuWindow &menu) ;
    void setScore();

private slots:


    void on_hearBtn_clicked();

    void on_startBtn_clicked();

    void on_tryBtn_clicked();
    void updatefreq();



private:
    Ui::hearingtestwindow *ui;
    QTimer *timer;
    bool isHearing;
    QMediaPlayer *sweep;
    QString filePath;
    int freq;

    //agregado
    MenuWindow *menu ;
    double maxScore ;
};

#endif // HEARINGTESTWINDOW_H
