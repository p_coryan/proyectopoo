#ifndef REACTIONTIMEWINDOW_H
#define REACTIONTIMEWINDOW_H

#include <QWidget>
#include <QTimer>
#include <QRandomGenerator>
#include <QElapsedTimer>
#include <string>
#include <QString>

class MenuWindow ;
namespace Ui {
class ReactionTimeWindow;
}

class ReactionTimeWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ReactionTimeWindow(QWidget *parent = nullptr);
    ~ReactionTimeWindow();
    QTimer *timer;
    QElapsedTimer *stopWatch1;

    void setMenu(MenuWindow &menu) ;
    void setScore() ;


private slots:
    void on_BtnStart_clicked();

private:
    Ui::ReactionTimeWindow *ui;

    //Atributos
    QRandomGenerator myRand; // see https://doc.qt.io/qt-5/qrandomgenerator.html
    bool isGreen, isRed;

    //Métodos
    void countdownDone();

    //sobrecargas
    void closeEvent(QCloseEvent *bar);

    //agreagdo
    MenuWindow *menu ;
    double maxscore ;


};

#endif // REACTIONTIMEWINDOW_H
