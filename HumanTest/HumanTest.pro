QT       += core gui
QT       += multimedia
QT       += charts
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    aimtestwindow.cpp \
    circlemove.cpp \
    hearingtestwindow.cpp \
    main.cpp \
    menuwindow.cpp \
    numbermemwindow.cpp \
    reactiontimewindow.cpp \
    user.cpp \
    userwindow.cpp \
    visualmemorywindow.cpp

HEADERS += \
    aimtestwindow.h \
    circlemove.h \
    hearingtestwindow.h \
    menuwindow.h \
    numbermemwindow.h \
    reactiontimewindow.h \
    user.h \
    userwindow.h \
    visualmemorywindow.h

FORMS += \
    aimtestwindow.ui \
    hearingtestwindow.ui \
    menuwindow.ui \
    numbermemwindow.ui \
    reactiontimewindow.ui \
    userwindow.ui \
    visualmemorywindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Sounds.qrc \
    images.qrc
