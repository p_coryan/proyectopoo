#include "reactiontimewindow.h"
#include "ui_reactiontimewindow.h"
#include "reactiontimewindow.h"
#include "menuwindow.h"
#include <QDebug>

#define BASETIME_MS 2000.0 //cuantos ms si o si
#define RANDOM_MS 3000.0 //cuantos ms por encima del basetime
#define BASEWAIT_MS 1000.0

ReactionTimeWindow::ReactionTimeWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReactionTimeWindow)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    timer->setSingleShot(true);
    myRand = QRandomGenerator::securelySeeded();

    isGreen = false;

    stopWatch1 = new QElapsedTimer();
    stopWatch1->start();

    //set the start message for the button.
    QColor col = QColor(Qt::white);
    QString qss = QString("background-color: %1").arg(col.name());
    ui->BtnStart->setStyleSheet(qss);
    ui->BtnStart->setText("Reaction Time test\nClick to start:");
}

ReactionTimeWindow::~ReactionTimeWindow()
{
    delete ui;
}

void ReactionTimeWindow::on_BtnStart_clicked()
{
    //this->setScore() ;
    if(isGreen){
        isGreen = false;
        std::string passTestMsg = "Hecho!\n"
                                  "Tu tiempo fue de " + std::to_string(stopWatch1->elapsed()) + "ms\n"
                                  "Aprieta para repetir.";
        ui->BtnStart->setText(QString::fromStdString(passTestMsg));

        //cambiar el color a blanco
        QColor col = QColor(Qt::white);
        QString qss = QString("background-color: %1").arg(col.name());
        ui->BtnStart->setStyleSheet(qss);
        qDebug() << "hola a todos \n" ;
        qDebug() << (double)stopWatch1->elapsed() ;
        //agregado
        if((double)stopWatch1->elapsed() < maxscore){
            maxscore = (double)stopWatch1->elapsed();
            //qDebug() << maxscore ;
            this->menu->user->setScoreReactionTime(maxscore);
            this->menu->user->writeFileUser();
            this->setScore() ;
            this->menu->setChart();

        }
        return;
    }

    if (isGreen == false){

        //cambiar el color a rojo
        QColor col = QColor(Qt::red);
        QString qss = QString("background-color: %1").arg(col.name());
        ui->BtnStart->setStyleSheet(qss);
        isRed = true;

        ui->BtnStart->setText("Espera a que el boton se ponga verde...");

        //poner a andar el timer por 4 + random +-1seg
        double r_ms=myRand.generateDouble()*RANDOM_MS;
        r_ms = r_ms+BASETIME_MS;
        timer->singleShot(r_ms, this, &ReactionTimeWindow::countdownDone);
        return;
    }

}

void ReactionTimeWindow::countdownDone(){
    //cambiar el color a verde
    QColor col = QColor(Qt::green);
    QString qss = QString("background-color: %1").arg(col.name());
    ui->BtnStart->setStyleSheet(qss);

    ui->BtnStart->setText("VERDE");
    isGreen = true;
    isRed = false;
    //QElapsedTime

    stopWatch1->restart();

}

void ReactionTimeWindow::closeEvent(QCloseEvent *bar){
    isGreen = false;

    //cambiar el color a blanco
    QColor col = QColor(Qt::white);
    QString qss = QString("background-color: %1").arg(col.name());
    ui->BtnStart->setStyleSheet(qss);
    ui->BtnStart->setText("Reaction Time test\nClick to start:");
}


void ReactionTimeWindow::setMenu(MenuWindow &menu){
    this->menu= &menu ;
}

void ReactionTimeWindow::setScore(){
    this->maxscore =  this->menu->user->getScoreRectionTime() ;
}

