#ifndef NUMBERMEMWINDOW_H
#define NUMBERMEMWINDOW_H

#include <QWidget>
#include <QRandomGenerator>
#include <QString>
#include <string>
#include <QTimer>


class MenuWindow ;
namespace Ui {
class NumberMemWindow;
}

class NumberMemWindow : public QWidget
{
    Q_OBJECT

public:
    explicit NumberMemWindow(QWidget *parent = nullptr);
    ~NumberMemWindow();
    void setLevel();
    void setOpenGame() ;
    void setMenu(MenuWindow &menu) ;
    void setScore() ;

private slots:
    void on_BtnOK_clicked();

    void on_BtnStart_clicked();

    void on_BtnRestart_clicked();

    void on_BtnExit_clicked();

public slots:
    void setProgressBar();


private:
    Ui::NumberMemWindow *ui;
    int countLvl ;
    int countBar ;
    QString numberLvl ;
    QRandomGenerator myRand;
    QTimer *timer;
    MenuWindow *menu ;
    double MaxScore ;


};

#endif // NUMBERMEMWINDOW_H
