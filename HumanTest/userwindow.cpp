#include "userwindow.h"
#include "ui_userwindow.h"
#include <QDir>
#include <QDebug>
#include <QString>
#include <QFile>
#include <QFileInfo> // para ver infiormacion de un archivo
#include "user.h"

UserWindow::UserWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserWindow)
{
    ui->setupUi(this);
}

UserWindow::~UserWindow()
{
    delete ui;
}

void UserWindow::setHome(MenuWindow &MainW){
    this->MainW = &MainW ;
}

void UserWindow::on_BtnEntrar_clicked()
{
    QString user =  ui->lineEditUser->text() ;
    QString fileUser =  user + ".txt" ;
    QString path ="C:/HumanTest/" + fileUser;
    QFileInfo check_file(path);
    if (check_file.exists() && check_file.isFile()){
        ui->labelErrorfile->setText("") ;
        this->close() ;
        MainW->show() ;
        // poner el path como argumento para abrirlo  en la clase usuario
        MainW->user->setNameUser(user) ;
        MainW->setNameLabelUser();
        MainW->user->setPathUser(path) ;
        MainW->user->readFileUser();
        MainW->setChart();

    }else{
        ui->labelErrorfile->setText(" Este usuario no existe ") ;
    }




}


void UserWindow::on_BtnNewUser_clicked()
{

    ui->labelErrorfile->setText("") ;
    QString user =  ui->lineEditUser->text() ;
    QString fileUser =  user + ".txt" ;

    QString path ="C:/HumanTest/";
    QDir dir;
    QFile file(path + fileUser);
    if(!dir.exists(path))
    {
        dir.mkpath(path);
        qDebug()<<"directory now exists";
        if ( file.open(QIODevice::ReadWrite) )
            {
                qDebug()<<"file now exists";
            }

    }else{
        if ( file.open(QIODevice::ReadWrite) )
            {
                qDebug()<<"file now exists";
            }

    }

    MainW->user->setNameUser(user) ;
    MainW->setNameLabelUser();
    MainW->user->resetScore() ;
    MainW->user->setPathUser(path + fileUser) ;
    MainW->user->writeFileUser() ;
    MainW->setChart();

    this->close() ;
    MainW->show() ;

    // pasarle el path al usuraio como argumento (a la clase )  ;

}

