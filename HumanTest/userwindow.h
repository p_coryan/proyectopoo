#ifndef USERWINDOW_H
#define USERWINDOW_H

#include <QWidget>
#include "menuwindow.h"
#include "user.h"

namespace Ui {
class UserWindow;
}

class UserWindow : public QWidget
{
    Q_OBJECT

public:
    explicit UserWindow(QWidget *parent = nullptr);
    ~UserWindow();
    void setHome(MenuWindow  &MainW) ;

private slots:
    void on_BtnEntrar_clicked();

    void on_BtnNewUser_clicked();

private:
    Ui::UserWindow *ui;
    MenuWindow * MainW ;


};

#endif // USERWINDOW_H
