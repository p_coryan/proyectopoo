#include "aimtestwindow.h"
#include "ui_aimtestwindow.h"
#include "menuwindow.h"

aimtestwindow::aimtestwindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::aimtestwindow)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    ui->BtnReset->setVisible(false);
    ui->resultText->setVisible(false);
    time = 0;
    count = 0;
    myRand = QRandomGenerator::securelySeeded();
    timer->setInterval(10);
    connect(timer,SIGNAL(timeout()),this,SLOT(timeUpdate()));
    isPlaying= false;
    isFinished = false;
    r = 50;  x = myRand.generateDouble()*150; y = myRand.generateDouble()*150;
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    circle = new Circlemove(0,0,r,r);
    scene->addItem(circle);
    circle->hide();





}

aimtestwindow::~aimtestwindow()
{
    delete ui;
    delete timer;
    delete scene;
    delete circle;
}

void aimtestwindow::mousePressEvent(QMouseEvent *event){
    if(!isFinished){
        if(!isPlaying) {
            Start();
            isPlaying = true;
            circle->move(x,y);
    }
        else{
            if(((event->x()-200-circle->x())*(event->x()-200-circle->y()) +
                (event->y()-200-circle->y())*(event->y()-200-circle->y()))<r*r){
            count++;
            ui->scoreText->setText(QString::number(count) +"/30");
            x = myRand.generateDouble()*150; y = myRand.generateDouble()*150;
            circle->move(x,y);
            }

        }

    }
}

void aimtestwindow::Restart(){
    isPlaying = false;
    isFinished = false;
    count = 0;
    time = 0;

    x = myRand.generateDouble()*150; y = myRand.generateDouble()*150;
    ui->BtnReset->setVisible(false);
    ui->resultText->setVisible(false);
    ui->scoreText->setText(QString::number(count) +"/30");
    ui->lcdNumber->display(time/1000);
    circle->move(x,y);
    circle->hide();

}

void aimtestwindow::Start(){
    timer->start();
    circle->show();




}
void aimtestwindow::timeUpdate(){
    time = time+10;
    ui->lcdNumber->display(time/1000);
    if(count==30){
        timer->stop();
        circle->hide();
        isFinished = true;
        ui->BtnReset->setVisible(true);
        ui->resultText->setVisible(true);
        ui->resultText->setText("Tu tiempo promedio fue: " + QString::number(time/30)+"ms");
        if((time/30) < maxScore){
            maxScore = time/30 ;
            this->menu->user->setScoreAIM(maxScore);
            this->menu->user->writeFileUser();

            this->menu->setChart();
        }
    }


}
/*void aimtestwindow::drawCircle(QPainter *painter){
    x = myRand.generateDouble();
    y = myRand.generateDouble();
    painter->setPen(Qt::red);
    painter->setBrush(Qt::red);
    painter->drawEllipse(x*350, y*350,
                         r,r);
}


void aimtestwindow::paintEvent(QPaintEvent *e){
    Q_UNUSED(e);
    QPainter painter(this);
    drawCircle(&painter);
}

*/
void aimtestwindow::on_BtnReset_clicked()
{
    this->setScore();
    Restart();

}

void aimtestwindow::setMenu(MenuWindow &menu){
    this->menu = &menu ;
}

void aimtestwindow::setScore(){
    maxScore = this->menu->user->getScoreAIM() ;
}

