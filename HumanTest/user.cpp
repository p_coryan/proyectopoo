#include "user.h"

User::User()
{
   this->scoreHearingTest = 23000 ;
    this->scoreNumberMem = 0 ;
    this->scoreReactionTime = 1200 ;
    this->scoreAIM=1000 ;

}

void User::readFileUser(){
    ifstream fileUser ;
    std::string path_string = path.toLocal8Bit().constData(); // para poder usar .open ya que no permite Qstring
    fileUser.open(path_string) ;
    fileUser >>  this->scoreHearingTest ;
    fileUser >>  this->scoreNumberMem ;
    fileUser >> this->scoreReactionTime ;
    fileUser >> this->scoreAIM ;


}

void User::writeFileUser(){
    std::string path_string = path.toLocal8Bit().constData(); // para poder usar .open ya que no permite Qstring
    ofstream os(path_string);
    os << this->scoreHearingTest << endl ;
    os << this->scoreNumberMem << endl ;
    os << this->scoreReactionTime << endl ;
    os << this->scoreAIM << endl ;


}

void User::setPathUser(QString path){
    this->path = path ;
}

void User::resetScore(){
     this->scoreHearingTest = 23000 ;
     this->scoreNumberMem = 0 ;
     this->scoreReactionTime = 1200 ;
     this->scoreAIM= 1000 ;
}

void User::setNameUser(QString nameUser){
    this->nameUser=nameUser ;
}

QString User::getNameUser(){
    return this->nameUser ;
}

double User::getScoreNumberMem(){
    return this->scoreNumberMem ;
}

void User::setScoreNumberMem(double newscore){
    scoreNumberMem = newscore ;
}

double User::getScoreHearingTest(){
    return this->scoreHearingTest ;
}

void User::setScoreHearingTest(double newscore){
    scoreHearingTest = newscore ;
}

double User::getScoreRectionTime(){
    return this->scoreReactionTime ;
}

void User::setScoreReactionTime(double newscore){
    scoreReactionTime = newscore ;
}

double User::getScoreAIM(){
    return this->scoreAIM ;
}

void User::setScoreAIM(double newscore){
    scoreAIM = newscore ;
}
