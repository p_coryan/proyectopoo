#ifndef VISUALMEMORYWINDOW_H
#define VISUALMEMORYWINDOW_H

#include <QWidget>

namespace Ui {
class VisualMemoryWindow;
}

class VisualMemoryWindow : public QWidget
{
    Q_OBJECT

public:
    explicit VisualMemoryWindow(QWidget *parent = nullptr);
    ~VisualMemoryWindow();

private:
    Ui::VisualMemoryWindow *ui;
};

#endif // VISUALMEMORYWINDOW_H
